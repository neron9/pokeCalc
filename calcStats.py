def getStats(base, iv, ev, level, nature):
    # print(base, iv, ev, level, nature)
    stats = ((((2 * base + iv + (ev / 4)) * level) / 100) + 5) * nature
    return(stats)


def getHP(base, iv, ev, level):
    stats = (((2 * base + iv + (ev / 4)) * level) / 100) + level + 10
    return(stats)