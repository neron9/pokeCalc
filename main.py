from listPokemon import *
from calcStats import *
import PySimpleGUI as sg

# This is a sample Python script.
import math
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
#if __name__ == '__main__':

#sg.theme('SandyBeach')
while True:
    layout = [
        [sg.Text('Choose your Pokemon!')],
        #[sg.Combo(['rattata', 'ratticate'])],
        [sg.Combo(allPokemon, size=(20,10))],
        [sg.Text('What stat do you wanna calculate?')],
        [sg.Listbox(['HP', 'attack','defense','specialattack','specialdefense','speed'], size=(20,5), enable_events=False)],
    #    [sg.Combo([allPokemon])],
    #    [sg.Text('What stat?', size =(15, 1)), sg.InputText()],
        [sg.Text('IV', size =(5, 1)), sg.InputText()],
        [sg.Text('EV', size =(5, 1)), sg.InputText()],
        [sg.Text('Level', size =(5, 1)), sg.InputText()],
    #    [sg.Text('Nature', size =(15, 1)), sg.InputText()],
        [sg.Text('What Nature does your Pokemon have?')],
        [sg.Listbox(['Positive', 'Neutral', 'Negative'], size=(40,3))],
        [sg.Submit(), sg.Cancel()]
    ]

    window = sg.Window('Pokemon Stat Calculator', layout)
    event, values = window.read()
    values[1] = str(values[1])
    values[1] = values[1].replace('[','')
    values[1] = values[1].replace("'","")
    values[1] = values[1].replace(']','')

    values[0] = str(values[0])
    values[0] = values[0].replace('[','')
    values[0] = values[0].replace("'","")
    values[0] = values[0].replace(']','')

    values[5] = str(values[5])
    values[5] = values[5].replace('[','')
    values[5] = values[5].replace("'","")
    values[5] = values[5].replace(']','')

    if values[5] == 'Positive':
        globals()['nature'] = 1.1
    if values[5] == 'Neutral':
        globals()['nature'] = 1.0
    if values[5] == 'Negative':
        globals()['nature'] = 0.9


    #poke = eval(values[0])
    #stat = getattr(poke, values[1])
    stat = getPokemonStat(values[0], values[1])
    if values[1] == 'HP':
        calculatedStats = getHP(stat, int(values[2]), int(values[3]), int(values[4]))
        output = "The HP of the Pokemon is " + str(calculatedStats)
    else:
        calculatedStats = getStats(stat, int(values[2]), int(values[3]), int(values[4]), float(nature))
        output = "The " + str(values[1]) + " of the Pokemon is " + str(calculatedStats)
    sg.Popup(output)
    window.close()

